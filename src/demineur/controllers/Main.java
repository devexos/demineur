package demineur.controllers;

import demineur.models.MineLand;
import demineur.models.Position;
import demineur.views.MineLandView;
import demineur.views.UserInteractionView;

public class Main {

    public static void main(String[] args) {
        // Creation du modèle
        MineLand mineLand = new MineLand();

        // Creation de la vue du terrain
        MineLandView view = new MineLandView(mineLand);
        view.display();

        // Creation de la vue d'interaction
        UserInteractionView uiView = new UserInteractionView();
        do {
            Position position = uiView.getNewPosition();

            // Interaction avec le modèle
            mineLand.play(position);
            view.display();
        } while (!mineLand.isUnmined() && !mineLand.isExploded());
        if (mineLand.isExploded()) {
            uiView.fail();
        } else {
            uiView.success();
        }
    }
}
