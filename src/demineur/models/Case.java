package demineur.models;

public class Case {
    protected boolean hasMine = false;
    protected boolean isShown = false;

    public boolean hasMine() {
        return hasMine;
    }

    public void toggleHasMine() {
        hasMine = !hasMine;
    }

    public void show() {
        isShown = true;
    }

    public boolean isShown() {
        return isShown;
    }
}
