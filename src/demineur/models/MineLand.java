package demineur.models;

public class MineLand {
    public static final int NB_LINES = 10;
    public static final int NB_COLUMNS = 20;
    private static final int NB_MINES = 80;

    protected boolean isFirstTurn = true;
    protected Case[][] grid;

    public MineLand() {
        grid = new Case[NB_COLUMNS][NB_LINES];
        for (int line = 0; line < NB_LINES; line++) {
            for (int column = 0; column < NB_COLUMNS; column++) {
                grid[column][line] = new Case();
            }

        }
    }

    private Case getCaseAt(Position position) {
        return grid[position.getX()][position.getY()];
    }

    public void play(Position position) {
        if (isFirstTurn) {
            isFirstTurn = false;
            setMines(position);
        }
        clearGridFrom(position);
        getCaseAt(position).show();
    }

    private void clearGridFrom(Position position) {
        if (getCaseAt(position).isShown()) {
            return;
        }
        if (getCaseAt(position).hasMine()) {
            return;
        }
        getCaseAt(position).show();
        Position[] positionsToVerify = new Position[]{
                position.withDelta(0, 1),
                position.withDelta(0, -1),
                position.withDelta(1, 0),
                position.withDelta(-1, 0)
        };
        for (Position positionToVerify : positionsToVerify) {
            if (isCorrectPosition(positionToVerify)) {
                clearGridFrom(positionToVerify);
            }
        }
    }

    private boolean isCorrectPosition(Position p) {
        return p.getX() >= 0 && p.getY() >= 0
                && p.getX() < NB_COLUMNS && p.getY() < NB_LINES;
    }

    private void setMines(Position firstPosition) {
        for(int nbMines = 0; nbMines < NB_MINES; nbMines++) {
            int line = 0;
            int column = 0;
            do {
                line = (int) Math.floor(Math.random() * NB_LINES);
                column = (int) Math.floor(Math.random() * NB_COLUMNS);
            }while(
                    firstPosition.equals(new Position(column, line))
                            || getCaseAt(new Position(column, line)).isShown()
                    || getCaseAt(new Position(column, line)).hasMine()
            );
            getCaseAt(new Position(column, line)).toggleHasMine();
        }
    }

    public boolean isShown(Position position) {
        return getCaseAt(position).isShown();
    }

    public boolean hasMine(Position position) {
        return getCaseAt(position).hasMine();
    }

    public int nbMinesAround(Position position) {
        int nbMines = 0;
        for (int line = -1; line <= 1; line++) {
            for (int column = -1; column <= 1; column++) {
                Position newPosition = position.withDelta(column, line);
                if (isCorrectPosition(newPosition) && hasMine(newPosition)) {
                    nbMines++;
                }
            }
        }
        return nbMines;
    }

    public boolean isUnmined() {
        for (Case[] line : grid) {
            for (Case aCase : line) {
                if (!aCase.isShown() && !aCase.hasMine())
                    return false;
            }
        }
        return true;
    }

    public boolean isExploded() {
        for (Case[] line : grid) {
            for (Case aCase : line) {
                if (aCase.isShown() && aCase.hasMine())
                    return true;
            }
        }
        return false;
    }
}
