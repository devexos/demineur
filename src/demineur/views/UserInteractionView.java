package demineur.views;

import demineur.models.Position;

import java.util.Scanner;

public class UserInteractionView {
    public Position getNewPosition() {
        System.out.println("Quelle position voulez-vous jouer ?");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        return new Position(x, y);
    }

    public void fail() {
        System.out.println("Perdu !");
    }

    public void success() {
        System.out.println("Gagné !");
    }
}
