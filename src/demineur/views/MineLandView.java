package demineur.views;

import demineur.models.MineLand;
import demineur.models.Position;

public class MineLandView {
    private final MineLand mineLand;

    public MineLandView(MineLand mineLand) {
        this.mineLand = mineLand;
    }

    public void display() {
        displayRowOfNumbers();
        for(int i = 0; i < MineLand.NB_LINES; i++) {
            displayLine(i);
        }
    }

    private void displayRowOfNumbers() {
        System.out.print("    ");
        for(int i = 0; i < MineLand.NB_COLUMNS; i++) {
            System.out.format("%3d", i);
        }
        System.out.println();
        System.out.print("____");
        for (int i = 0; i < MineLand.NB_COLUMNS; i++) {
            System.out.print("___");
        }
        System.out.println();
    }

    private void displayLine(int line) {
        System.out.format("%3d |", line);
        for(int column = 0; column < MineLand.NB_COLUMNS; column++) {
            Position position = new Position(column, line);
            if (mineLand.isShown(position)) {
                displayShownCase(position);
            } else {
                System.out.print(" # ");
            }
        }
        System.out.println();
    }

    private void displayShownCase(Position position) {
        if (mineLand.hasMine(position)) {
            System.out.print("***");
        } else {
            int nbMines = mineLand.nbMinesAround(position);
            if (nbMines == 0)
                System.out.print("   ");
            else
                System.out.format(" %d ", nbMines);
        }
    }

}
